class Sequences:
    def __init__(self):
        pass

    @staticmethod
    def task1():
        lst = [int(s) for s in input().split(" ")]
        for i in lst:
            if (i % 2) == 1:
                print(i, end=" ")

    @staticmethod
    def task2():
        lst = [int(s) for s in input().split(" ")]
        i = 1
        while i < len(lst):
            if lst[i] > lst[i-1]:
                print(lst[i], end=" ")
            i += 1

    @staticmethod
    def task3():
        lst = [int(s) for s in input().split(" ")]
        i = 0
        while i+2 < len(lst):
            t = lst[i]
            lst[i] = lst[i+1]
            lst[i+1] = t
            i += 2
        print(lst)

    @staticmethod
    def task4():
        lst = [int(s) for s in input().split(" ")]
        maxi = lst[0]
        mini = lst[0]
        i = 0
        while i < len(lst):
            if lst[i] < mini:
                mini = i
            if lst[i] > maxi:
                maxi = i
            i += 1
        t = lst[mini]
        lst[mini] = lst[maxi]
        lst[maxi] = t
        print(lst)

    @staticmethod
    def task5():
        lst1 = [int(s) for s in input().split(" ")]
        lst2 = [int(s) for s in input().split(" ")]
        s = 0
        for i in lst1:
            for j in lst2:
                if i == j:
                    s += 1
                    break
        print(s)

    @staticmethod
    def task6():
        lst1 = [int(s) for s in input().split(" ")]
        lst2 = [int(s) for s in input().split(" ")]
        for i in lst1:
            for j in lst2:
                if i == j:
                    print(i, end=" ")
                    break

    @staticmethod
    def task7():
        lst = [int(s) for s in input().split(" ")]
        lst1 = []
        for i in lst:
            if lst1.count(i) != 0:
                print("ДА")
            else:
                print("НЕТ")
                lst1.append(i)

    @staticmethod
    def task8():
        x = input()
        print(x[2])
        print(x[-1])
        print(x[0:5])
        print(x[:len(x)-2])
        print(x[:len(x):2])
        print(x[1:len(x):2])
        x_rev = x[::-1]
        print(x_rev)
        print(x_rev[:len(x_rev):2])

    @staticmethod
    def task9():
        x = input()
        len1 = len(x)
        if len1 % 2 == 1:
            x1 = x[:(len1//2)+1]
            x2 = x[(len1//2)+1:]
        else:
            x1 = x[:(len1 // 2)]
            x2 = x[(len1 // 2):]
        print(x2, x1, sep="")

    @staticmethod
    def task10():
        x = input()
        h1 = x.index("h", 0, len(x))
        h2 = x.rindex("h", h1+1, len(x))
        x2 = x[:h1]
        x3 = x[h2+1:]
        print(x2, x3, sep="")

    @staticmethod
    def task11():
        x = input()
        h1 = x.index("h", 0, len(x))
        h2 = x.rindex("h", h1 + 1, len(x))
        x_new = x[h1+1:h2]
        xnew_rev = x_new[::-1]
        x2 = x[:h1+1]
        x3 = x[h2:]
        print(x2, xnew_rev, x3, sep="")

    @staticmethod
    def task12():
        x = input()
        h1 = x.index("h", 0, len(x))
        h2 = x.rindex("h", h1 + 1, len(x))
        x2 = x[:h1 + 1]
        x3 = x[h2:]
        xnew = x[h1+1:h2]
        xnew1 = xnew.replace("h", "H")
        print(x2, xnew1, x3, sep="")
