class CycleWhile:
    def __init__(self):
        pass

    @staticmethod
    def task1():
        x = int(input())
        i = 2
        while i <= x:
            if x % i == 0:
                print(i)
                break
            i += 1

    @staticmethod
    def task2():
        x = int(input())
        n = 0
        while 2**(n+1) < x:
            n += 1
        print(n, 2**n)

    @staticmethod
    def task3():
        x = float(input())
        y = float(input())
        i = 1
        while x < y:
            x *= 1.1
            i += 1
        print(i)

    @staticmethod
    def task4():
        i = 1
        s = 0
        while i != 0:
            i1 = int(input())
            if i1 > i:
                s += 1
            i = i1
        print(s)

    @staticmethod
    def task5():
        n = int(input())
        i = 1
        while i**2 < n:
            print(i**2)
            i += 1

    @staticmethod
    def task6():
        n = int(input())
        i1 = 1
        i2 = 1
        i = 2
        while n >= i2:
            if i2 == n:
                print(i)
                break
            t = i2
            i2 = i2+i1
            i1 = t
            i += 1
        if n < i2:
            print(-1)

    @staticmethod
    def task7():
        n = int(input())
        i1 = 1
        i2 = 1
        i = 2
        while n != i:
            t = i2
            i2 = i2 + i1
            i1 = t
            i += 1
        print(i2)

    @staticmethod
    def task8():
        i1 = int(input())
        i2 = int(input())
        s = 1
        s_max = 1
        while i2 != 0:
            if i1 == i2:
                s += 1
                if s > s_max:
                    s_max = s
            else:
                s = 1
            t = i2
            i2 = int(input())
            i1 = t
        print(s_max)
