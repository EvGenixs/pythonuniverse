class IOTheme():
    def __init__(self):
        pass

    @staticmethod
    def task1():
        x = int(input())
        y = int(input())
        z = int(input())
        print(x + y + z)

    # Задание 2
    @staticmethod
    def task2():
        h = int(input())
        a = int(input())
        print((h * a) / 2)

    # Задание 3
    @staticmethod
    def task3():
        name = input()
        print("Привет, " + name + "!")

    # Задание 4
    @staticmethod
    def task4():
        x = int(input())
        print("Следующее число для числа " + str(x) + ": " + str(x + 1))
        print("Предыдущее число для числа " + str(x) + ": " + str(x - 1))

    # Задание 5
    @staticmethod
    def task5():
        s = int(input())
        a = int(input())
        s1 = int(a / s)
        k1 = int(a % s)
        print(f"Яблок у студентов: {s1}, в корзине: {k1}")

    # Задание 6
    @staticmethod
    def task6():
        N = int(input())
        h = (N // 3600) % 23
        m = N // 60
        print(h, m)

    # Задание 7
    @staticmethod
    def task7():
        h1 = int(input())
        m1 = int(input())
        s1 = int(input())
        h2 = int(input())
        m2 = int(input())
        s2 = int(input())
        dif = (h2 * 3600 + m2 * 60 + s2) - (h1 * 3600 + m1 * 60 + s1)
        print(dif)