class Numbers():
    def __init__(self):
        pass

    @staticmethod
    def task1():
        x = int(input())
        print(x // 10, x % 10)

    @staticmethod
    def task2():
        x = int(input())
        print(((x % 10)*10)+(x // 10))

    @staticmethod
    def task3():
        x = int(input())
        print(x % 100)

    @staticmethod
    def task4():
        x = int(input())
        d = (x % 100) // 10
        print(d)

    @staticmethod
    def task5():
        x = int(input())
        print(int(x//100 + ((x % 100) // 10) + (x % 10)))

    @staticmethod
    def task6():
        x = float(input())
        print((int(x * 10) % 10))

    @staticmethod
    def task7():
        x = int(input())
        print(((x-1) // 100) + 1)

    @staticmethod
    def task8():
        N = int(input())
        print((N+3) % 7)

    @staticmethod
    def task9():
        x = int(input())
        h = (x // 60) % 24
        m = x % 60
        print(f'{h}:{m}')

    @staticmethod
    def task10():
        c1 = int(input())
        c2 = int(input())
        c3 = int(input())
        print(int(((c1+1) // 2) + ((c2+1) // 2) + ((c3+1) // 2)))