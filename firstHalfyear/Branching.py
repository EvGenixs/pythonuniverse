class Branching:
    def __init__(self):
        pass

    @staticmethod
    def task1():
        x = int(input())
        if x % 2 == 1:
            print("нечет")
        else:
            print("чет")

    @staticmethod
    def task2():
        a = int(input())
        b = int(input())
        print(min(a, b))

    @staticmethod
    def task3():
        x = int(input())
        if x > 0: print(1)
        if x == 0: print(0)
        if x < 0: print(-1)

    @staticmethod
    def task4():
        x = int(input())
        if (x > 99) and (x < 1000):
            print("Да")
        else:
            print("Нет")

    @staticmethod
    def task5():
        a = int(input())
        b = int(input())
        if (a >= 0) or (b >= 0):
            print("Да")
        else:
            print("Нет")

    @staticmethod
    def task6():
        x = int(input())
        h = x // 100
        d = (x - (h*100)) // 10
        o = x % 10
        if(h < d) and (d < o) and (h < o):
            print("Да")
        else:
            print("Нет")

    @staticmethod
    def task7():
        x = int(input())
        t = x // 1000
        h = (x % 1000) // 100
        d = (x % 100) // 10
        o = x % 10
        if(t == o) and (h == d):
            print("Да")
        else:
            print("Нет")

    @staticmethod
    def task8():
        year = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}
        x = int(input())
        print(year[x])

    @staticmethod
    def task9():
        x1 = int(input())
        x2 = int(input())
        x3 = int(input())
        if (x1 == x2) and (x1 == x3):
            print(3)
        elif (x1 == x2) or (x1 == x3) or (x2 == x3):
            print(2)
        else:
            print(0)

    @staticmethod
    def task10():
        i = int(input())
        j = int(input())
        if (i+j) % 2 == 0:
            print("Черная")
        else:
            print("Белая")

    @staticmethod
    def task11():
        i = int(input())
        j = int(input())
        i2 = int(input())
        j2 = int(input())
        if (((i+j) % 2 == 0) and ((i2+j2) % 2 == 0)) or (((i+j) % 2 == 1) and ((i2+j2) % 2 == 1)):
            print("ДА")
        else:
            print("Нет")

    @staticmethod
    def task12():
        year = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}
        m = int(input())
        d = int(input())
        if (year[m] > d):
            print(f'{d+1}-{m}-2022')
        else:
            print(f'1-{m+1}-2022')





