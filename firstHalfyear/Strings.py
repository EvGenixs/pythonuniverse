class Strings():
    def __init__(self):
        pass

    @staticmethod
    def task1():
        x = input()
        print((x.count(" ", 0, x.__sizeof__()))+1)

    @staticmethod
    def task2():
        x = input()
        words = x.rsplit(" ")
        print(words[1], words[0])

    @staticmethod
    def task3():
        x = input()
        f_first = x.find("f", 0, x.__sizeof__())
        print(f_first)
        f_last = x.rfind("f", f_first+1, x.__sizeof__())
        print(f_last)

    @staticmethod
    def task4():
        x = input()
        x_new = x.replace("1", "one")
        print(x_new)

    @staticmethod
    def task5():
        x = str(input())
        print(x.strip('@'))

