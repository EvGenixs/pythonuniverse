class DictionaryTheme:
    def __init__(self):
        pass

    @staticmethod
    def task1():
        lst = [str(s) for s in input().split(" ")]
        lst1 = []
        for i in lst:
            if lst1.count(i) == 0:
                lst1.append(i)
                print(0, end=" ")
            else:
                lst1.append(i)
                print(lst1.count(i)-1, end=" ")

    @staticmethod
    def task2():
        n = int(input())
        dict1 = {}
        for i in range(n):
            lst = [str(s) for s in input().split(" ")]
            dict1[lst[0]] = lst[1]
        key1 = input()
        print(dict1[key1])

    @staticmethod
    def task3():
        n = int(input())
        dict1 = {}
        for i in range(n):
            lst = [str(s) for s in input().split(" ")]
            try:
                dict1[lst[0]] = int(dict1[lst[0]]) + int(lst[1])
            except:
                dict1[lst[0]] = int(lst[1])
        print(dict1)

    @staticmethod
    def task4():
        n = int(input())
        dict1 = {}
        for i in range(n):
            lst = [str(s) for s in input().split(" ")]
            for j in lst:
                try:
                    dict1[j] = dict1[j]+1
                except:
                    dict1[j] = 1
        max_dict = {k:v for k, v in dict1.items() if v == max(dict1.values())}
        print(sorted(max_dict.items()))

    @staticmethod
    def task5():
        n1 = int(input())
        dict1 = {}
        for i in range(n1):
            lst = [str(s) for s in input().split(" ")]
            dict1[lst[0]] = list(lst[1:])
        n2 = int(input())
        for i in range(n2):
            lst1 = [str(s) for s in input().split(" ")]
            testlst = dict1[lst1[1]]
            if testlst.count("R") != 0:
                testlst.remove("R")
                testlst.append("read")
            if testlst.count("W") != 0:
                testlst.remove("W")
                testlst.append("write")
            if testlst.count("X") != 0:
                testlst.remove("X")
                testlst.append("execute")
            if testlst.count(lst1[0]) != 0:
                print("OK")
            else:
                print("Denied")

    @staticmethod
    def task6():
        n1 = int(input())
        dict1 = {}
        for i in range(n1):
            lst = [str(s) for s in input().split(" ")]
            dict1[lst[0]] = list(lst[1:])
        n2 = int(input())
        ans = []
        for i in range(n2):
            city = input()
            for k, v in dict1.items():
                if dict1[k].count(city) != 0:
                    ans.append(k)
        print(ans)

    @staticmethod
    def task7():
        n1 = int(input())
        dict1 = {}
        for i in range(n1):
            lst = [str(s) for s in input().split(" ")]
            for j in lst:
                try:
                    dict1[j] = dict1[j] + 1
                except:
                    dict1[j] = 1
        print(sorted(dict1.values(), reverse=True))





















